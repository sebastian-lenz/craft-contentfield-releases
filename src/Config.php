<?php

namespace lenz\contentfield;

use craft\base\Model;

/**
 * Class Config
 */
class Config extends Model
{
  /**
   * @var string
   */
  public $googleMapsApiKey;

  /**
   * @var string
   */
  public $googleTranslateApiKey;
}
