<?php

namespace lenz\contentfield\models\fields\enumerations;

/**
 * Class SelectField
 *
 * Displays a textarea input.
 */
class SelectField extends AbstractEnumerationField
{
  /**
   * The internal name of this widget.
   */
  const NAME = 'select';
}
