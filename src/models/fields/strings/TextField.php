<?php

namespace lenz\contentfield\models\fields\strings;

/**
 * Class TextField
 *
 * Displays a textarea input.
 */
class TextField extends AbstractStringField
{
  /**
   * The internal name of this widget.
   */
  const NAME = 'text';
}
