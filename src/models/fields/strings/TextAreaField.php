<?php

namespace lenz\contentfield\models\fields\strings;

/**
 * Class TextAreaField
 *
 * Displays a textarea input.
 */
class TextAreaField extends AbstractStringField
{
  /**
   * The internal name of this widget.
   */
  const NAME = 'textarea';
}
